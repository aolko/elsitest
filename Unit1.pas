unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Vcl.ComCtrls, JvExComCtrls, JvDBTreeView, Data.FMTBcd, Vcl.StdCtrls,
  Datasnap.DBClient, Datasnap.Provider, SimpleDS, Data.DBXMySQL, Data.Win.ADODB,
  System.ImageList, Vcl.ImgList, Vcl.ExtCtrls, Vcl.ToolWin, Vcl.ActnMan,
  Vcl.ActnCtrls, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.Grids, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    TreeView1: TTreeView;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ImageList1: TImageList;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    tb_refresh: TToolButton;
    ImageList2: TImageList;
    Splitter1: TSplitter;
    Panel2: TPanel;
    labelTextRight: TLabel;
    ToolButton1: TToolButton;
    tb_about: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure tb_refreshClick(Sender: TObject);
    procedure TreeView1DblClick(Sender: TObject);
    procedure tb_aboutClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  CurrentGroupID, RecordGroupID, i: Integer;
  RootNode, GroupNode, ItemNode: TTreeNode;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  RootNode := TreeView1.Items.Add(nil, '[�������]');
  labelTextRight.Caption := '�������� �������� �������� � ������ ��������.'+sLineBreak+'�������� ������ ��� ������ ������.';
end;

procedure TForm1.tb_aboutClick(Sender: TObject);
begin
  showmessage('������������ �������� ����. ������ Lite.');
end;

procedure TForm1.tb_refreshClick(Sender: TObject);
begin
  labelTextRight.Caption := '';
  TreeView1.Items.Clear;
  RootNode:= TreeView1.Items.Add(nil, '[�������]');
  CurrentGroupID := 0;
  GroupNode := nil;
  ADOQuery1.SQL.Text := 'SELECT * from items order by id';
  ADOQuery1.Open;
  try
    ADOQuery1.First;
    while not ADOQuery1.Eof do
    begin
      RecordGroupID := ADOQuery1.FieldByName('id').AsInteger;
      if (GroupNode = nil) or (RecordGroupID <> CurrentGroupID) then
      begin
        GroupNode := TreeView1.Items.AddChild(RootNode, ADOQuery1.FieldByName('parent').AsString);
        GroupNode.ImageIndex:=1;
        GroupNode.SelectedIndex:=GroupNode.ImageIndex;
        CurrentGroupID := RecordGroupID;
      end;
      ItemNode := TreeView1.Items.AddChild(GroupNode, ADOQuery1.FieldByName('name').AsString);
      ItemNode.ImageIndex:=2;
      ItemNode.SelectedIndex:=ItemNode.ImageIndex;
      ADOQuery1.Next;
    end;
  finally
    ADOQuery1.Close;
    treeview1.FullExpand;
  end;
end;

procedure TForm1.TreeView1DblClick(Sender: TObject);
begin
if Assigned(TreeView1.Selected) then
    //begin
    //  if SameText(TreeView1.Selected.Text, '[�������]') then
    //   ShowMessage(TreeView1.Selected.Count.ToString);
    //  end;
    ShowMessage(TreeView1.Selected.Count.ToString);
    end;
end.


